# Media Embed Container

Media Embed Container is a simple module intended to wrap a media
embed in a div with a max-width attribute

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_embed_container).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/media_embed_container).


## Requirements

- Core Media module
- Text editor that uses media library embed


## Recommended Modules

- [easy_responsive_images](https://www.drupal.org/project/easy_responsive_images)
- This module does a lot on its own. When used with media_embed_container it can
  be used in the ckeditor to allow editors to change max-width per embed.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration pages

1. Config page: `/admin/config/media/media_embed_container`
2. Used for configuring what media bundles and view modes to apply to.


## Maintainers

- Stephen Mustgrave - [smustgrave](https://www.drupal.org/u/smustgrave)
